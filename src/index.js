// markup
import './index.html'

// fonts
import 'typeface-fira-sans'
import 'typeface-fira-sans-extra-condensed'

// stylesheet
import './index.scss'

document.addEventListener('DOMContentLoaded', () => {
  const elements = require('./elements.yml')
  elements.forEach(element => {
    let htmlElement = document.querySelector(`.element[data-number="${element.number}"]`)
    htmlElement.innerHTML = `
      <span class="number">${element.number}</span>
      <span class="weight">${element.weight}</span>
      <span class="symbol ${element.state}">${element.symbol}</span>
      <span class="name${element.radioactive === true ? ' element.radioactive' : ''}">${element.name}</span>
      <span class="melt">${element.melt}</span>
      <span class="boil">${element.boil}</span>
      <span class="density">${element.density}</span>
      <span class="electronegativity">${element.electronegativity}</span>
    `
    element.group.split(' ').forEach(group => {
      htmlElement.classList.add(group)
    })
    if (element.radioactive === true) {
      htmlElement.querySelector('.name').classList.add('radioactive')
    }
  })
})
