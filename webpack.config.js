const StyleLintPlugin = require('stylelint-webpack-plugin');
const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'public')
  },
  plugins: [
    new StyleLintPlugin({})
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          'eslint-loader'
        ]
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
        ]
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.html$/,
        use: [
          'file-loader?name=[name].[ext]',
          'extract-loader',
          'html-loader'
        ]
      },
      {
        test: /\.(yml|yaml)$/,
        use: [
          'json-loader',
          'yaml-loader'
        ]
      },
      {
        test: /\.(woff|woff2)$/,
        use: [
          'file-loader'
        ]
      },
      {
        test: /\.png$/,
        use: [
          'url-loader' // without limits, because we only use small images
        ]
      },
      {
        test: /\.svg$/,
        use: [
          'svg-url-loader'
        ]
      }
    ]
  }
};
