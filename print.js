const puppeteer = require('puppeteer')

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

(async() => {
const browser = await puppeteer.launch({
  args: [
    '--no-sandbox',
    '--disable-setuid-sandbox',
    '--allow-file-access-from-files'
  ]
})

const page = await browser.newPage()
await page.goto('file://' + process.cwd() + '/public/index.html', {waitUntil: 'networkidle2'})
await page.pdf({
  path: 'public/index.pdf',
  format: 'A4',
  landscape: true,
  scale: 0.65,
  margin: {
    top: '0.5cm',
    right: '0.5cm',
    bottom: '0.5cm',
    left: '0.5cm'
  }
})

await browser.close()
})()
